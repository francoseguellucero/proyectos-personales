import matplotlib.pyplot as plt 
promedios_generacion = []
cantidad_ind = []
tiempos_promedio = []


archivo = open("datos.csv","r")
linea = archivo.readline()
while(linea != ""):
	datos = linea.split(",")
	promedios_generacion.append(float(datos[0]))
	cantidad_ind.append(float(datos[1]))
	tiempos_promedio.append(float(datos[2])/1000)
	linea = archivo.readline()
archivo.close()
#Funcion para calcular el minimo
def min(array):
	minn = array[0]
	index = 0
	for i in range(len(array)):
		if array[i] < minn:
			minn = array[i]
			index = i
	return (minn,index)
tupla = min(tiempos_promedio)
print("Tiempo min [ms]: ", tupla[0])
print("Cantidad de indiv optimos: ", cantidad_ind[tupla[1]])

plt.subplot(2,1,1)

plt.plot(cantidad_ind,promedios_generacion)
plt.xlabel("Cantidad de individuos")
plt.ylabel("Promedios de generaciones")
plt.title("Cantidad de generaciones promedio por número de individuos")
plt.grid(True)

plt.subplot(2,1,2)
plt.plot(cantidad_ind,tiempos_promedio)
plt.xlabel("Cantidad de individuos")
plt.ylabel("Tiempo promedio [s]")
plt.title("Tiempo promedio por cantidad de individuos")
plt.grid(True)

plt.show()

