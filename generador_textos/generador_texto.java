import java.lang.Math;
import java.util.*;
class Utilidades{
	public static double suma(int[] array){
		double sum=0;
		for(int i = 0; i < array.length ; i++){
			sum = sum + array[i]*1.0;
		}
		return sum;
	}
	public static long suma(long[] array){
		long sum=0;
		for(int i = 0; i < array.length ; i++){
			sum = sum + array[i];
		}
		return sum;
	}
	public static int[] max(int[] array){
		int maximo, indice;
		maximo = 0;
		indice = 0;
		for(int i = 0 ; i < array.length ; i++){
			if(maximo < array[i]){
				maximo = array[i];
				indice = i;
			}
		}
		int [] resp = {maximo,indice};
		return resp;
	}
	public static double promedio(int[] array){
		double sum = suma(array)*1.0;
		return sum/(array.length*1.0);
	}
	
	public static long promedio(long[] array){
		long sum = suma(array);
		return sum / (array.length);

	}
	public static int[] enumerar(int[] array, int inicio, int intervalo){
		for( int i = 0; i < array.length; i++){
			array[i] = inicio + intervalo * i;
		}
		return array;
	}
}

class Individuo{
	String adn;
	Individuo(String adn){
		this.adn=adn;
	}
	String getADN(){
		return this.adn;
	}	
}
class Poblacion{
	Individuo[] poblacion;
	int[] ranking;
	int cant_indiv;
	String caracteres;
	Poblacion(int cantidad_individuos, Individuo padre, Individuo madre){
		poblacion = new Individuo[cantidad_individuos];
		caracteres = "1234567890'¡¿?=)(/&%$·!ª)}][{¬½~#@|}]QWERTYUIOPASDFGHJKLÑZXCVBNMqwertyuiopasdfghjklñzxcvbnm ,.-;:_·̣̣̣̣̣`+´ç^*¨Ç<>";
		for( int j = 0; j < cantidad_individuos ; j++){
			
			String adn = "";
			
			//generar el adn 
			for(int i = 0; i < madre.getADN().length(); i++){
				
				//cruzamiento aleatorio entre progenitores.
				double rnd = Math.random();
				if(rnd < 0.4){
					adn = adn + madre.getADN().charAt(i);
				}
				else if(rnd < 0.8){
					adn = adn + padre.getADN().charAt(i);
				}
				else{
					Random randomInt = new Random();
					adn = adn + caracteres.charAt(randomInt.nextInt(caracteres.length()));
				}
			}

			//crear el individuo y agregarlo a la poblacion
			poblacion[j] = new Individuo(adn);
		}

		cant_indiv = cantidad_individuos;
	}
	//entrega el ranking de un individuo
	public int rankIndiv(Individuo indiv,String texto){
		int ranking = 0;

		//recorrer todo el adn para calcular el ranking
		for( int j = 0; j < indiv.getADN().length() ; j++){
				
			if(indiv.getADN().charAt(j) == texto.charAt(j)){
				
				ranking++;
			
			}
		}
		return ranking;
	}
	//modifica el campo ranking de la clase
	void Ranking(String texto){
		
		ranking = new int[poblacion.length];
		//recorrer todos los individuos de la poblacion asociandole un ranking a cada uno
		for(int i = 0; i < poblacion.length ; i++){			
			ranking[i]= rankIndiv(poblacion[i], texto);
		}

	}
	// max: selecciona el maximo de un arreglo, retorna el numero maximo y el indice de este.
	
	
	Individuo[] SeleccionarMejores(String texto){
		//rankear la poblacion
		Ranking(texto);

		//suma del arreglo de rankings
		
		double sum = Utilidades.suma(ranking);
		if(sum == 0.0){
			Random rand = new Random();
			int indice_padre = rand.nextInt(cant_indiv); //seleccionar al padre
			int indice_madre = rand.nextInt(cant_indiv); //seleccionar a la madre
			Individuo[] resp = {poblacion[indice_padre],poblacion[indice_madre]};
			return resp;
		}
		else{
			/*Escoger el maximo, setearlo en 0 y luego escoger el segundo maximo */
			int[] padre_e_indice = Utilidades.max(ranking); //padre
			ranking[padre_e_indice[1]] = 0;
			int[] madre_e_indice = Utilidades.max(ranking);//madre
			Individuo[] resp = {poblacion[padre_e_indice[1]], poblacion[madre_e_indice[1]]};
			return resp;
		}
	}

}

public class generador_texto{
	static String generarTexto(int tamano){
		String caracteres = "1234567890'¡¿?=)(/&%$·!ª)}][{¬½~#@|}]QWERTYUIOPASDFGHJKLÑZXCVBNMqwertyuiopasdfghjklñzxcvbnm ,.-;:_·̣̣̣̣̣`+´ç^*¨Ç<>";
		String texto = "";
		for(int i = 0 ; i < tamano ; i++){
			int indice = (int) (Math.random() * caracteres.length());
			texto = texto + (""+caracteres.charAt(indice));	
		}
		return texto;


	}
	public static void main(String[] args){
		String texto_a_generar = "Yo quiero ser ingeniero";
		int tamano_texto_a_generar = texto_a_generar.length(); 
	
		int[] cantidad_individuos = new int[818]; 
		double[] promedios_generacion = new double[818]; 
		long[] promedios_tiempo = new long[818]; 
		cantidad_individuos = Utilidades.enumerar(cantidad_individuos, 200, 20);
		for( int j =0 ; j < cantidad_individuos.length ; j++){ //recorre cantidad de individuos
			int[] datos_generacion = new int[100];
			long[] tiempo_promedio = new long[100];

			for( int i = 0; i < datos_generacion.length; i++){ //recorre los datos de la generacion

				long inicio = System.currentTimeMillis( );
				//generar ADN de los progenitores 
				String adnP = generarTexto(tamano_texto_a_generar);
				String adnM = generarTexto(tamano_texto_a_generar);
				

				Individuo padre = new Individuo(adnP);
				Individuo madre = new Individuo(adnM);
				
				int fitness = 0;
				Individuo[] progenitores = {padre, madre};
				int generacion = 0;
				while(fitness < 1){
					Poblacion poblacion = new Poblacion(cantidad_individuos[j] , progenitores[0] , progenitores[1]);
					progenitores =  poblacion.SeleccionarMejores(texto_a_generar);
					fitness = poblacion.rankIndiv(progenitores[0], texto_a_generar) / tamano_texto_a_generar;

					generacion++;
				}
				long termino = System.currentTimeMillis( );
				long diff = termino - inicio;
				datos_generacion[i] = generacion;
				tiempo_promedio[i] = diff;

			}
			promedios_generacion[j]=Utilidades.promedio(datos_generacion);
			promedios_tiempo[j] = Utilidades.promedio(tiempo_promedio);
			System.out.println(promedios_generacion[j]+","+cantidad_individuos[j]+","+promedios_tiempo[j]);
		}
		
	}
}
