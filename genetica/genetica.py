class MonoHibridismo:
    def __init__(self,madre,padre):
        assert len(madre)==2 and len(padre)==2
        assert type(madre)== str and type(padre)== str
        self.__madre=madre
        self.__padre=padre
    def getMadre(self):
        return self.__madre
    def getPadre(self):
        return self.__padre
    def cruza(self):
        gametosPadre=[self.__padre[0],self.__padre[1]]
        gametosMadre=[self.__madre[0],self.__padre[1]]
        self.__resultado=[]
        AA=0
        Aa=0
        aa=0
        for i in range(len(gametosPadre)):
            for j in range(len(gametosMadre)):
                self.__resultado.append(gametosPadre[i]+gametosMadre[j])
        if gametosPadre[0]!=gametosPadre[1]:
        
            if (gametosPadre[0]+gametosPadre[0]) in self.__resultado:
                for i in range(len(self.__resultado)):
                    if self.__resultado[i]== (gametosPadre[0]+gametosPadre[0]):
                        AA += 1
            if (gametosPadre[0]+gametosPadre[1]) in self.__resultado :
                for i in range(len(self.__resultado)):
                    if self.__resultado[i]==(gametosPadre[0]+gametosPadre[1]):
                        Aa += 1
            if (gametosPadre[1]+gametosPadre[1]) in self.__resultado:
                for i in range(len(self.__resultado)):
                    if self.__resultado[i]==(gametosPadre[1]+gametosPadre[1]):
                        aa += 1
        
        self.__dicc={"AA": AA,"Aa":Aa,"aa":aa}
    def mostrarProporcion(self):
        print("AA: "+str(self.__dicc["AA"]/len(self.__resultado))+"%")
        print("Aa: "+str(self.__dicc["Aa"]/len(self.__resultado))+"%")
        print("aa: "+str(self.__dicc["aa"]/len(self.__resultado))+"%")
